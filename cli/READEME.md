# xsl/@cli 说明

## 目标

- 易复用 的模型抽象
- 约定大于规范 的目录结构的扩展方式
- 命令式 的使用方式
- 交互式 的使用方式

## 下一步（V1.0.0 RELEASE）

- major
  - modules 抽离出相关 domain
  - 可配置化相关 
  - 响应式相关

- minor
  - rxjs 整合
  - logger 优化

## 模块

- git
  - config
    - alias
      - **简介**: 团队常用 git 命令的短命令设置。
    - profile
      - **简介**： 进行个人 git.user.{name,email} 信息的设定。
  - clone
    - **简介**： 进行个人 git projects 的快速操作。

## 模块依賴

### yargs

#### 简介

Yargs 是一个提供了参数解析功能并能自动生成优雅的用户接口的，帮助用户构建 interactive模式的命令行工具的库。

#### 特性

- 参数解析
- 自动生成优雅的用户接口，如：help 选项等的自动生成
- 模块化子命令支持
- bash下的自动补全
- 等等

Github 地址：[https://github.com/yargs/yargs](https://github.com/yargs/yargs)

### execa —— 更好的 [`child_process`](https://nodejs.org/api/child_process.html) 替代

#### 特性

- Promise 接口.
- [Strips EOF](https://github.com/sindresorhus/strip-eof) from the output so you don't have to `stdout.trim()`.
- 跨平台的支持 [shebang](https://en.wikipedia.org/wiki/Shebang_(Unix)) binaries
- [windows 支持提升](https://github.com/IndigoUnited/node-cross-spawn#why)
- Higher max buffer. 10 MB instead of 200 KB.
- 运行本地命令.
- [清除父进程已结束的 spawned 进程](#cleanup)

Github 地址：[https://github.com/sindresorhus/execa](https://github.com/sindresorhus/execa)

### inquirer —— 响应式的用户命令行接口

#### 特性

- 提供错误反馈
- 问题询问模式
- 输入转换
- 回答验证
- 多层 prompts 管理

Github 地址：[https://github.com/SBoudrias/Inquirer.js](https://github.com/SBoudrias/Inquirer.js)  

## 模块编写

- 参考 `./modules` 目录结构，编写模块化命令模块

  ```
  modules
     ├── cli.js
     ├── git
     │   ├── clone.const.js
     │   ├── clone.js
     │   ├── clone.op.js
     │   ├── config
     │   ├── config.const.js
     │   ├── config.js
     │   └── debug.js
     ├── git.js
     └──  index.js
  
  ```
- 参考 `./cli.js` 和`./cli.bootstrap.js` 编写启动入口文件
  - **注意**： 下次迭代计划使用 `CommadModule对象` 代替目前零散的结构。
- 参考 `./index.js` 编写启动文件

