'use strict';

const { Command } = require('./domains/command/index');
const { Cli } = require('./domains/cli');

const cli = require('./cli');

const cliCommand = new Command({ name: cli.command, description: cli.description, isOrigin: true })
  .register(cli);
const cliBootstrap = new Cli(cliCommand);

module.exports = cliBootstrap;
