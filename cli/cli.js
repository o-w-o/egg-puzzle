'use strict';

const { logger } = require('./utils/logger');
const { gitModule } = require('./modules');

const command = 'shx';

const description = '执行 自定义命令';

const symbol = 'shx';

const builder = {
  options: {
    interactive: {
      alias: 'i',
      description: '交互式界面',
      type: 'string',
    },
  },
  subCommands: {
    [gitModule.command]: gitModule,
  },
  subCommandsHelper: {
    [gitModule.command]: {
      description: gitModule.description,
    },
  },
};

const handler = function(argv) {
  logger.o('script/cli.js').$.debug(`${symbol} argv -> %o`, argv);
  logger.x();
};

module.exports = {
  command,
  description,
  builder,
  handler,
  symbol,
};
