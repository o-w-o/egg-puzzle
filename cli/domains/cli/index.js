'use strict';

const yargs = require('yargs');

const { Command } = require('../command/index');
const { logger } = require('../../utils/logger');

const { parseScript } = require('./parser');

class Cli extends Command {
  constructor(entryCommand) {
    super(entryCommand);
    this.subCommands = entryCommand.subCommands;
    this.options = entryCommand.options;
    this.replMap = new Map();
    this.replStack = [];
    this.replCurrent = this;

    this.init();
  }

  init() {
    this.yargs = yargs.scriptName(this.name);
    this.mount(this.yargs, this);

    return this;
  }

  async exec() {
    if (this.isInteractiveMode()) {
      logger.$.info('exec');
      await this.execInteractiveInquirer().then(
        answers => {
          this.generateScript(answers).then(
            script => {
              this.execArgv(parseScript(script));
            }
          );
        }
      );
    } else {
      logger.$.info('execScript');
      await this.execArgv(process.argv.slice(2));
    }
  }

  execArgv(argv) {
    this.yargs.parse(argv);
  }

  isInteractiveMode() {
    const firstArgs = process.argv[2];
    return (firstArgs === '-i' || firstArgs === '--interactive');
  }


  /**
   * 尝试匹配一个Token对应的一个子命令:subCommands或选项:option
   * 如果有,返回该对象
   * 反之,返回false
   * @param {String} token 令牌
   */
  adapt(token) {
    if (this.adapter.has(token)) {
      this.adapter.get(token).accept();
    }
  }

  inquire(questions, handler, errorHandler) {
    this.inquirer
      .prompts(questions)
      .then(handler)
      .catch(e => {
        errorHandler(e);
      });
  }

  async execInteractiveInquirer() {
    const answers = await this.inquirer
      .prompt([
        {
          type: 'autoComplete',
          name: 'sample',
          message: '请选择要执行的命令 >> ',
          source: async (answers, input) => {
            return await this.autoComplete(input);
          },
        },
      ])
      .then(answers => {
        logger.$.info(answers);
      })
      .catch(e => {
        logger.$.error(e);
      });

    return answers;
  }

  async autoComplete(input) {
    const completion = [];

    this.options.forEach((val, key) => {
      if (key.indexOf(input)) completion.push(key);
    });

    this.subCommands.forEach((val, key) => {
      if (key.indexOf(input)) completion.push(key);
    });

    return completion;
  }

  async generateScript(script) {
    return script;
  }
}

exports.Cli = Cli;
