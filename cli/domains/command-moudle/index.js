'use strict';

class CommandModule {
  constructor() {
    this.command;
    this.description;
    this.builder;
    this.handler;
    this.symbol;
  }
}

exports.CommandModule = CommandModule;
