'use strict';

const inquirer = require('inquirer');
const inquirerAutoCompletePrompt = require('inquirer-autocomplete-prompt');
inquirer.registerPrompt('autoComplete', inquirerAutoCompletePrompt);

const { logger } = require('../../utils/logger');

const { parseModule } = require('./parser');

logger.$n('command.js');

/**
 * 命令
 */
class Command {
  /**
   * -
   * @param {String} name 命令名
   * @param {String} description 命令描述
   * @param {String} interactiveSymbol 交互式命令中供以快速定位命令的 **短**介绍
   */
  constructor({ name, description, symbol, isOrigin = false }) {
    this.isOrigin = isOrigin;

    // 命令基本属性
    this.name = name;
    this.description = description;
    this.options = new Map();
    this.subCommands = new Map();

    // 命令附加属性
    this.symbol = symbol;

    // 日志记录
  }

  register(module) {
    const { command, subCommands, options } = parseModule(module, this);

    this.registerCommand(command, subCommands, options);

    return this;
  }

  /* 注入 命令的源命令和父命令 上下文 */
  injectCommandContext(originCommand, parentCommand) {
    this.originCommand = originCommand;
    this.parentCommand = parentCommand;

    return this;
  }

  registerHandler(handler) {
    this.handler.set(handler.id, handler);
  }

  registerInquirer(inquirer) {
    this.adapter.set(inquirer.symbol, inquirer);
  }

  registerCommand({ name, usage, description, handler }, subCommands, options) {
    this.name = name;
    this.usage = usage;
    this.description = description;
    this.handler = handler;

    this.registerOptions(options);
    this.registerSubCommands(subCommands);

    return this;
  }

  registerOptions(optionsMap) {
    optionsMap.forEach((option, optionKey) => {
      this.registerOption(option, optionKey);
    });
    return this;
  }

  /* 注册一个参数到当前命令 */
  registerOption(option, optionKey) {

    if (this.options.has(optionKey)) {
      logger.$.warn("Duplicated args's option -> %o, when exec [registerOption]!", optionKey);
    }

    logger.$.debug([ 'register', optionKey, 'option', '', this.name ]);
    this.options.set(optionKey, option);

    return this;
  }

  registerSubCommands(subCommandsMap) {
    subCommandsMap.forEach((subCommand, subCommandKey) => {
      this.registerSubCommand(subCommand, subCommandKey);
    });
    return this;
  }

  registerSubCommand(subCommand, subCommandKey) {
    const {
      command,
      subCommands,
      options,
    } = subCommand;

    subCommand = new Command(command)
      .registerCommand(command, subCommands, options);

    // TODO 完善 registerSubCommand 校验
    this.subCommands.set(subCommandKey, subCommand);
  }

  mount(yargs) {
    this.options.forEach((option, optionKey) => {
      logger.$.debug('mount key -> %s', optionKey);
      this.mountOption(optionKey, option, yargs);
    });
    this.subCommands.forEach((subCommand, subCommandKey) => {
      logger.$.debug('mount subCommandKey -> %o', subCommandKey);
      this.mountSubCommand(subCommand, subCommandKey, yargs);
    });
    return this;
  }

  mountOption(option, optionConfig, yargs) {
    yargs.option(option, optionConfig);
  }

  mountSubCommand(subCommand, subCommandKey, yargs) {
    const {
      name,
      description,
      handler,
      options,
      subCommands,
    } = subCommand;

    logger.$.debug({
      name,
      description,
    });

    yargs.command(name, description, _yargs => {
      if (options.size > 0) {
        options.forEach((option, optionKey) => {
          _yargs.option(optionKey, option);
        });
      }

      if (subCommands.size > 0) {
        subCommands.forEach((_subCommand, _subCommandKey) => {
          return this.mountSubCommand(
            _subCommand,
            _subCommandKey,
            _yargs
          );
        });
      }
    }, handler);

    return yargs;
  }
}

exports.Command = Command;

logger.$u();
