/* eslint-disable no-use-before-define */
'use strict';

const chalk = require('chalk');
const { createStream, getBorderCharacters } = require('table');
const { logger } = require('../../utils/logger');

const parseTrackerSwitcher = false;

const parseTrackerConstructor = () => {
  const header = [
    chalk.bold('Action'),
    chalk.bold('Val'),
    chalk.bold('Key'),
    chalk.bold('Self'),
    chalk.bold('Parent'),
    chalk.bold('Handler'),
  ];
  const config = {
    columnDefault: {
      width: 20,
    },
    columnCount: 6,
    border: getBorderCharacters('ramac'),
    columns: {
      0: {
        alignment: 'left',
        width: 25,
      },
      1: {
        width: 10,
      },
      2: {
        width: 10,
      },
      3: {
        width: 10,
      },
      4: {
        width: 20,
      },
      5: {
        width: 50, wrapWord: true,
      },
    },

  };
  const stream = createStream(config);
  stream.write(header);
  return stream;
};
const parseTracker = parseTrackerSwitcher ? parseTrackerConstructor().write : () => {};

const parseOptions = (options, target) => {
  const optionsMap = new Map();

  const keys = Object.keys(options);
  if (keys.length > 0) {
    keys.forEach(optionKey => {
      optionsMap.set(optionKey, options[optionKey]);

      parseTracker([ 'parse', optionKey, 'option', '', `${target.name}:yargs`, '' ]);
    });
  }

  return optionsMap;
};

const parseSubCommands = (subCommands, target) => {
  logger.$.debug(subCommands);
  const subCommandsMap = new Map();

  const keys = Object.keys(subCommands);
  if (keys.length > 0) {
    keys.forEach(subCommandKey => {
      const subCommand = subCommands[subCommandKey];
      const { command: _command, subCommands: _subCommands, options: _options } = parseModule(subCommand, target);
      parseTracker([ 'parse', _command.name, 'subCommand', '', target.name, _command.handler ]);

      subCommandsMap.set(subCommandKey, {
        name: subCommandKey,
        command: _command,
        subCommands: _subCommands,
        options: _options,
      });
    });
  }
  return subCommandsMap;
};

const parseModule = (module, target) => {
  const {
    command: name,
    usage,
    description,
    symbol,
    builder: { options, subCommands },
    handler,
  } = module;

  const command = { name, usage, description, symbol, options, subCommands, handler };
  const subCommandsMap = parseSubCommands(subCommands, command);
  const optionsMap = parseOptions(options, command);

  parseTracker([
    `register ${chalk.bold.greenBright('constructor')}`,
    name,
    'command',
    name,
    chalk.underline.bgGreen.whiteBright.bold(target ? target.name : name),
    handler,
  ]);


  return {
    command,
    subCommands: subCommandsMap,
    options: optionsMap,
  };
};


exports.parseModule = parseModule;
