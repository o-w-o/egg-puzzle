#!/usr/bin/env node

'use strict';


(async () => {
  await require('./cli.bootstrap').exec()
    .catch(e => {
      console.error(e);
    });
})();
