'use strict';

const { logger } = require('../utils/logger');
const config = require('./git/config');
const clone = require('./git/clone');
const debug = require('./git/debug');

const command = 'git';

const description = '执行 git 自定义命令';

const symbol = 'git-module';

const builder = {
  options: {
    debug: {
      alias: 'D',
      description: '测试使用选项',
      type: 'string',
      choices: [ 'a', 'b', 'c' ],
    },
  },
  subCommands: {
    [config.command]: config,
    [clone.command]: clone,
    [debug.command]: debug,
  },
  subCommandsHelper: {
    [config.command]: {
      description: config.description,
    },
    [clone.command]: {
      description: clone.description,
    },
  },
};

const handler = function(argv) {
  logger.$.debug(`${symbol} argv -> %o`, argv);
};

module.exports = {
  command,
  description,
  builder,
  handler,
  symbol,
};
