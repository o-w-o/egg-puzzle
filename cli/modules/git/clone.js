'use strict';

const { logger } = require('../../utils/logger');
const { cloneOperator } = require('./clone.op');

logger.$n('git/clone.js').$.info('read file >> git/clone.js');
logger.$u();

const command = 'clone';

const description = '通过配置和API调用快速克隆远程仓库';

const symbol = 'git-clone';

const builder = {
  options: {},
  subCommands: {},
};


const handler = async argv => {
  logger.$.debug(`${symbol} argv -> %o`, argv);

  if (new Set(argv._).has('clone')) {
    await cloneOperator();
  }
};

module.exports = {
  command,
  description,
  builder,
  handler,
  symbol,
};
