'use strict';

const { KEY } = require('./clone.const');
const { execScriptSet } = require('../../utils/script-executor');
const { getConfig } = require('../../utils/config-helper');

const { logger } = require('../../utils/logger');

const inquirer = require('inquirer');
const inquirerAutoCompletePrompt = require('inquirer-autocomplete-prompt');
inquirer.registerPrompt('autoComplete', inquirerAutoCompletePrompt);

const curl = require('urllib');
const configName = 'clone.conf.json';
let config;

const getGitLabCloneUrl = ({ gitlabUrl = 'code.xingshulin.com', privateToken = 'j4BAEp4N1KxUirnHPQxG' }) => {
  return `http://${gitlabUrl}/api/v3/projects/?private_token=${privateToken}`;
};

const getGitLabCloneSearchUrl = async ({ inputSegment }) => {
  return `${getGitLabCloneUrl(config)}&search=${inputSegment}`;
};

const initCloneConfigGenerator = async () => {
  const config = await inquirer
    .prompt([{
      name: 'gitlabUrl',
      message: '请输入 gitlab domain >> ',
      default: 'code.xingshulin.com',
    },

    {
      name: 'privateToken',
      message: '请输入 gitlab private-token >> ',
      default: 'j4BAEp4N1KxUirnHPQxG',
    },
    {
      name: 'storeBaseDir',
      message: '请输入 gitlab 本地存储地址 >> ',
      default: 'Projects',
    },
    ])
    .then(async answers => {
      logger.$.info('initCloneConfigGenerator config -> ', answers);
      return answers;
    })
    .catch(e => {
      logger.$$.error(e);
    });

  return config;
};

const fetchProjectsDataByQuery = async input => {
  const listProjectApiUrl = await getGitLabCloneSearchUrl({ inputSegment: input });

  return await curl.request(listProjectApiUrl, {
    headers: {
      'Content-Type': 'application/json',
    },
    contentType: 'json',
    dataType: 'json',
  })
    .then(result => {
      const projectsArr = [];
      const projectsMap = new Map();

      if (result.res.statusCode === 200) {
        const { data } = result;
        if (data && data.length > 0) {
          data.forEach(val => {
            const { path_with_namespace: projectName, description } = val;
            projectsMap.set(projectName, val);
            projectsArr.push(projectName + ' - ' + description);
          });
        }
        // logger.$$.info('status: %s, body size: %o, headers: %j', result.res.statusCode, result.data, result.res.headers);
      }
      logger.$.debug('project -> %o', projectsMap.size);

      return {
        arr: projectsArr,
        map: projectsMap,
      };
    })
    .catch(err => {
      console.error(err);
    });
};

const cloneInquirer = async () => {
  let projectsMap = new Map();
  let project = {};

  await inquirer
    .prompt([{
      type: 'autoComplete',
      name: 'projectName',
      message: '请选择要 clone 的远程仓库 >> ',
      source: async (answers, input) => {
        const { map, arr } = await fetchProjectsDataByQuery(input);
        projectsMap = map;
        return arr;
      },
    }])
    .then(answers => {
      const projectName = answers.projectName.split(' ')[0].trim();
      project = projectsMap.get(projectName);
    })
    .catch(e => {
      logger.$$.error(e);
    });

  logger.$.debug('project -> %o', project);
  return project;
};

const getProjectGitUrl = project => project[KEY.REPO_SSH_URL];
const getProjectStorePath = (config, project) => `${config.storeBaseDir}/${project.name}`;

const cloneOperator = async () => {
  if (!config) {
    const configBaseDir = require('path').join(__dirname, '');
    config = await getConfig(configBaseDir, configName, initCloneConfigGenerator);
  }
  const project = await cloneInquirer();
  const projectGitUrl = getProjectGitUrl(project);
  const projectStorePath = getProjectStorePath(config, project);

  await execScriptSet([ `git clone ${projectGitUrl} ~/${projectStorePath}` ]);
  logger.$$.info(`~/${projectStorePath}`);
};

exports.cloneOperator = cloneOperator;
