'use strict';

exports.SCOPE = {
  L: 'local',
  G: 'global',
  S: 'system',
};
