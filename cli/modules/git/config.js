'use strict';

const { logger } = require('../../utils/logger');
const alias = require('./config/alias');
const profile = require('./config/profile');

logger.$n('git/config/alias.js');

const command = 'config';

const description = '配置 git';

const symbol = 'git-config';

const builder = {
  options: {
    debug: {
      alias: 'D',
    },
  },
  subCommands: {
    [alias.command]: alias,
    [profile.command]: profile,
  },
};

const handler = function(argv) {
  logger.$.debug(`${symbol} argv -> %o`, argv);
};

module.exports = {
  command,
  description,
  builder,
  handler,
  symbol,
};

logger.$u();
