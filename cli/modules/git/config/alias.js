'use strict';

const { logger } = require('../../../utils/logger');
const { gitConfigAliasOperator } = require('./alias.op');
const { SCOPE } = require('../config.const');

logger.$n('git/config/alias.js').$.info('read file >> git/config/alias.js');
logger.$u();

const command = 'alias';

const description = '设置 git 命令别名';

const symbol = 'git-config-alias';

const builder = {
  options: {
    set: {
      alias: 's',
      default: true,
    },
    clear: {
      alias: 'c',
      default: false,
    },
    scope: {
      default: SCOPE.L,
      type: 'string',
      choices: [ SCOPE.L, SCOPE.S, SCOPE.G ],
    },
  },
  subCommands: {},
};

const handler = argv => {
  const option = {};
  if (argv.c || argv.clear) {
    logger.$.debug('args -> -c');
    option.unset = true;
  }

  if (argv['--scope']) {
    logger.$.debug('args -> --scope');
    option.scope = argv['--scope'];
  }

  gitConfigAliasOperator({ ...argv });
  logger.$.debug(`${symbol} argv -> %o`, { ...argv });
};

const inquirer = function() {
  // -
};

module.exports = {
  command,
  description,
  builder,
  handler,
  inquirer,
  symbol,
};
