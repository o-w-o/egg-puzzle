'use strict';
// const { logger } = require('../../../utils/logger');
const { execScriptSet } = require('../../../utils/script-executor');
const { SCOPE } = require('../config.const');


const genAliasScriptMap = () => {
  this.aliasScriptMap = new Map();
  this.aliasScriptMap.set('s', 'status');
  this.aliasScriptMap.set('su', 'checkout');
  this.aliasScriptMap.set('a', '!git add . && git status');
  this.aliasScriptMap.set('au', '!git add -u . && git status');
  this.aliasScriptMap.set('aa', '!git add . && git add -u . && git status');
  this.aliasScriptMap.set('ai', '!git add -i');
  this.aliasScriptMap.set('c', 'cz');
  this.aliasScriptMap.set('cm', 'commit -m');
  this.aliasScriptMap.set('ca', 'commit --amend');
  this.aliasScriptMap.set('ac', '!git add . && git commit');
  this.aliasScriptMap.set('acm', '!git add . && git commit -m');
  this.aliasScriptMap.set(
    'l',
    "log --graph --all --pretty=format:'%C(yellow)%h%C(cyan)%d%Creset %s %C(white)- %an, %ar%Creset'"
  );
  this.aliasScriptMap.set('ll', 'log --stat --abbrev-commit');
  this.aliasScriptMap.set(
    'lg',
    "log --color --graph --pretty=format:'%C(bold white)%h%Creset -%C(bold green)%d%Creset %s %C(bold green)(%cr)%Creset %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"
  );
  this.aliasScriptMap.set(
    'llg',
    "log --color --graph --pretty=format:'%C(bold white)%H %d%Creset%n%s%n%+b%C(bold blue)%an <%ae>%Creset %C(bold green)%cr (%ci)' --abbrev-commit"
  );
  this.aliasScriptMap.set('d', 'diff');
  this.aliasScriptMap.set('dc', 'diff --cached');
  this.aliasScriptMap.set('master', 'checkout master');

  return this.aliasScriptMap;
};

exports.gitConfigAliasOperator = async ({ unset = false, scope = SCOPE.L }) => {
  const scriptSet = new Set();
  const aliasScriptMap = genAliasScriptMap();

  aliasScriptMap.forEach((aliasCommand, aliasName) => {
    scriptSet.add(`git config ${unset ? '--unset' : ''} --${scope} alias.${aliasName} ${unset ? '' : `"${aliasCommand}"`}`);
  });

  await execScriptSet(scriptSet);
};

