'use strict';

const { logger } = require('../../../utils/logger');
const { gitConfigProfileOperator } = require('./profile.op');
const { SCOPE } = require('../config.const');

const command = 'profile';

const description = '设置 git 用户信息';

const symbol = 'git-config-alias';

const builder = {
  options: {
    scope: {
      default: SCOPE.L,
    },
  },
  subCommands: {},
};

const handler = argv => {
  const option = {};

  if (argv['--scope']) {
    logger.$.debug('args -> --scope');
    option.scope = argv['--scope'];
  }

  gitConfigProfileOperator(option);
  logger.$.debug(`${symbol} argv -> %o`, argv);
};

const inquirer = function() {
  // -
};

// exports.command = command;
// exports.description = description;
// exports.builder = builder;
// exports.handler = handler;
// exports.inquirer = inquirer;
// exports.symbol = symbol;

module.exports = {
  command,
  description,
  builder,
  handler,
  inquirer,
  symbol,
};
