'use strict';

const inquirer = require('inquirer');

// const { logger } = require('../../../utils/logger');
const { execScriptSet } = require('../../../utils/script-executor');
const { SCOPE } = require('../config.const');

const gitConfigProfileOperator = ({ scope = SCOPE.L }) => {
  inquirer
    .prompt([
      {
        type: 'input',
        name: 'name',
        message: '请输入您的名字：',
        validate(value) {
          const pass = value.match(
            /^([a-zA-Z0-9\u4e00-\u9fa5\·]{1,10})$/i
          );
          if (pass) {
            return true;
          }

          return '请输入一个正常点的名字';
        },
      },
      {
        type: 'input',
        name: 'email',
        message: '请输入您的邮箱地址：',
        validate(value) {
          const pass = value.match(
            /^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})$/i
          );
          if (pass) {
            return true;
          }

          return '请输入一个合法的邮箱';
        },
      },
    ])
    .then(async answers => {
      // logger.$$.log(JSON.stringify(answers, null, '  '));
      await execScriptSet([
        `git config ${scope} user.name ${answers.name}`, // 配置用户名
        `git config ${scope} user.email ${answers.email}`,
      ]);
    });
};

exports.gitConfigProfileOperator = gitConfigProfileOperator;
