'use strict';

const { logger } = require('../../utils/logger');

logger.$n('git/debug.js').$.info('read file >> git/debug.js');
logger.$u();

const command = 'debug';

const description = 'just for debug!';

const symbol = 'git-debug';

const builder = {
  options: {},
  subCommands: {},
};


const handler = async argv => {
  logger.o('modules/git/debug.js').$.info(`${symbol} argv -> %o`, argv);
  logger.x();
};

module.exports = {
  command,
  description,
  builder,
  handler,
  symbol,
};
