'use strict';

const fs = require('fs');
const findUp = require('find-up');

const { logger } = require('./logger');

const isExist = async (configBaseDir, configName = 'config.json') => {
  logger.$.debug(configBaseDir, configName);

  const configPath = await findUp(configName, {
    cwd: configBaseDir,
  });
  return configPath ? configPath : false;

};

const createConfigJSONFile = async configPath => {
  await fs.writeFile(configPath, '', 'utf8');
};

const writeDataToConfigJSONFile = async (configPath, data) => {
  await fs.writeFileSync(configPath, JSON.stringify(data, null, 2));
};

const readDataFromConfigJSONFile = async configPath => {
  const data = fs.readFileSync(configPath, 'utf8');
  return JSON.parse(data);
};


const getConfig = async (configBaseDir, configName, initConfigGenerator = () => {}) => {
  let config;
  let configFilePath = await isExist(configBaseDir, configName)
    .catch(e => {
      logger.$.error(e);
    });

  if (configFilePath) {
    config = await readDataFromConfigJSONFile(configFilePath);
  } else {
    configFilePath = configBaseDir + '/' + configName;
    await createConfigJSONFile(configFilePath)
      .then(async () => {
        await initConfigGenerator()
          .then(async configData => {
            if (configData) {
              await writeDataToConfigJSONFile(configFilePath, configData)
                .then(() => {
                  config = configData;
                })
                .catch(e => {
                  logger.$.error(e);
                });
            } else {
              throw new Error(configData);
            }

          })
          .catch(e => {
            logger.$.error(e);
          });
      })
      .catch(e => {
        logger.$.error(e);
      });
  }

  return config;
};

module.exports = {
  isExist,
  createConfigJSONFile,
  writeDataToConfigJSONFile,
  readDataFromConfigJSONFile,
  getConfig,
};
