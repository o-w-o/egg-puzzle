'use strict';

const logger = require('log4js');

class Logger {
  constructor() {
    this.logger = logger.getLogger();
    this.logger.level = 'debug';
  }

  setLevel(level) {
    this.logger.level = level;
  }

  $blank() {
    this.logger.log('');
  }

  $u() {
    this.logger = logger.getLogger();
  }

  $n(namespace) {
    this.logger = logger.getLogger(namespace);
    return this;
  }

  get $() {
    return this.status
      ? this.logger
      : {
        log: () => {},
        info: () => {},
        warn: () => {},
        error: () => {},
        debug: () => {},
      };
  }

  get $$() {
    return this.logger;
  }

  o(namespace) {
    this.$n(namespace);
    this.status = true;
    return this;
  }

  x() {
    this.$u();
    this.status = false;
    return this;
  }
}

exports.logger = new Logger();
