'use strict';

const exec = require('execa');
const chalk = require('chalk');
const { logger } = require('./logger');


/**
 * ScriptMap 结构  {
 *   key: commandStr,
 *   value: optionArr,
 * }
 * @param {Map} scripts 脚本Map
 */
const execScriptMap = async scripts => {

  logger.$.debug(scripts);
  await scripts.forEach(async (scriptValue, scriptKey) => {
    logger.o('script-executor').$.debug(`exec script -> ${scriptKey}  ${scriptValue}`);
    logger.x();
    await exec.shell(`${scriptKey}  ${scriptValue}`);
  });
};

exports.execScriptMap = execScriptMap;

const execScriptSet = async scripts => {
  logger.o('script-executor');
  await scripts.forEach(async script => {
    try {
      await exec.shell(script).then(result => {
        logger.$.info(`exec script -> ${script}, result -> %o`, result.stdout);
      });
    } catch (e) {
      logger.$.error(`
        exec script -> ${chalk.bold(script)}
        err info -> ${chalk.red.bold(e.stderr)}`);
    }
  });
  logger.x();
};

exports.execScriptSet = execScriptSet;
