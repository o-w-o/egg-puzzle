'use strict';

module.exports = {
  extends: [
    '@commitlint/config-conventional',
  ],
  rules: {
    'scope-enum': [ 0, 'always', []],
    'scope-case': [ 0, 'always', [ 'lower-case' ]],
    'subject-case': [ 0, 'always', []],
    'body-max-length': [ 0, 'always', 72 ],
    'type-enum': [
      2,
      'always',
      [
        'build',
        'chore',
        'ci',
        'docs',
        'feat',
        'fix',
        'perf',
        'refactor',
        'revert',
        'style',
        'test',
      ],
    ],
  },
};
